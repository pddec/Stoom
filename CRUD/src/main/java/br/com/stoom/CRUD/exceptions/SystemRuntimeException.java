package br.com.stoom.CRUD.exceptions;

import br.com.stoom.CRUD.enums.ErrorCodes;

public class SystemRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public SystemRuntimeException(ErrorCodes codes) {
		super(codes.name());
	}

}
